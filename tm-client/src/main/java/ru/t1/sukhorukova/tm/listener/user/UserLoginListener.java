package ru.t1.sukhorukova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.dto.request.user.UserLoginRequest;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.event.ConsoleEvent;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

@Component
public final class UserLoginListener extends AbstractUserListener {

    @NotNull private final String NAME = "user-login";
    @NotNull private final String DESCRIPTION = "Sign in.";

    @Override
    @EventListener(condition = "@userLoginListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER LOGIN]");

        System.out.println("Enter login:");
        @Nullable final String login = TerminalUtil.nextLine();

        System.out.println("Enter password:");
        @Nullable final String password = TerminalUtil.nextLine();

        @NotNull final UserLoginRequest request = new UserLoginRequest(getToken());
        request.setLogin(login);
        request.setPassword(password);

        @Nullable final String token = getAuthEndpoint().login(request).getToken();
        setToken(token);
        System.out.println(token);
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable @Override
    public Role[] getRoles() {
        return null;
    }

}
