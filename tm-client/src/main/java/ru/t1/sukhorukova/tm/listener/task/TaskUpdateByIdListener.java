package ru.t1.sukhorukova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.dto.request.task.TaskUpdateByIdRequest;
import ru.t1.sukhorukova.tm.event.ConsoleEvent;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-update-by-id";

    @NotNull
    public static final String DESCRIPTION = "Update task by id.";

    @Override
    @EventListener(condition = "@taskUpdateByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[UPDATE TASK BY ID]");

        System.out.println("Enter task id:");
        @Nullable final String taskId = TerminalUtil.nextLine();

        System.out.println("Enter task name:");
        @Nullable final String name = TerminalUtil.nextLine();

        System.out.println("Enter task description:");
        @Nullable final String description = TerminalUtil.nextLine();

        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(getToken());
        request.setTaskId(taskId);
        request.setName(name);
        request.setDescription(description);
        getTaskEndpoint().updateByIdTask(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
