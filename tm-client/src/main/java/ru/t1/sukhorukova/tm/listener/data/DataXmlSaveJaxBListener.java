package ru.t1.sukhorukova.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.dto.request.data.DataXmlSaveJaxBRequest;
import ru.t1.sukhorukova.tm.event.ConsoleEvent;

@Component
public final class DataXmlSaveJaxBListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-xml-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Save data in xml file.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlSaveJaxBListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE XML]");

        getDomainEndpoint().xmlSaveJaxBData(new DataXmlSaveJaxBRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
