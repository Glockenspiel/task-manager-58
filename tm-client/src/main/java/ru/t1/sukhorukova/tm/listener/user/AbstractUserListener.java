package ru.t1.sukhorukova.tm.listener.user;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.sukhorukova.tm.api.endpoint.IUserEndpoint;
import ru.t1.sukhorukova.tm.listener.AbstractListener;

@Component
public abstract class AbstractUserListener extends AbstractListener {

    @Autowired
    private IUserEndpoint userEndpoint;

    @Autowired
    private IAuthEndpoint authEndpoint;

    protected IUserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    protected IAuthEndpoint getAuthEndpoint() {
        return authEndpoint;
    }

    @Nullable @Override
    public String getArgument() {
        return null;
    }

}
