package ru.t1.sukhorukova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.dto.request.project.ProjectStartByIndexRequest;
import ru.t1.sukhorukova.tm.event.ConsoleEvent;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

@Component
public final class ProjectStartByIndexListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-start-by-index";

    @NotNull
    public static final String DESCRIPTION = "Start project by index.";

    @Override
    @EventListener(condition = "@projectStartByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[START PROJECT BY INDEX]");

        System.out.println("Enter project index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @Nullable final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(getToken());
        request.setIndex(index);
        getProjectEndpoint().startByIndexProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
