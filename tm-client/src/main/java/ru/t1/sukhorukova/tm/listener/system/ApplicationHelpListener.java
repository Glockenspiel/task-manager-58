package ru.t1.sukhorukova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.api.command.ICommand;
import ru.t1.sukhorukova.tm.event.ConsoleEvent;
import ru.t1.sukhorukova.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @NotNull
    public static final String ARGUMENT = "-h";

    @NotNull
    public static final String NAME = "help";

    @NotNull
    public static final String DESCRIPTION = "Show help.";

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[HELP]");
        for (@Nullable final AbstractListener listener : listeners) System.out.println(listener);
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
