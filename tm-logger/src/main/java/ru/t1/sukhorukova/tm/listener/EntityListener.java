package ru.t1.sukhorukova.tm.listener;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.service.EntityService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
@NoArgsConstructor
public final class EntityListener implements MessageListener {

    @NotNull
    @Autowired
    private EntityService loggerService;

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        @NotNull final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        @NotNull final String json = textMessage.getText();
        loggerService.log(json);
    }

}
