package ru.t1.sukhorukova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    EntityManager getEntityManager();

    void add(@NotNull M model);

    void update(@NotNull M model);

    void remove(@NotNull M model);

    @Nullable
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull String id);

    void removeOneById(@NotNull String id);

    long getSize();

}
