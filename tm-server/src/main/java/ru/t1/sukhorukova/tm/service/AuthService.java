package ru.t1.sukhorukova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.sukhorukova.tm.api.service.dto.ISessionDtoService;
import ru.t1.sukhorukova.tm.exception.user.NotLoggedInException;
import ru.t1.sukhorukova.tm.exception.user.PermissionException;
import ru.t1.sukhorukova.tm.dto.model.SessionDTO;
import ru.t1.sukhorukova.tm.api.service.IAuthService;
import ru.t1.sukhorukova.tm.api.service.IPropertyService;
import ru.t1.sukhorukova.tm.api.service.dto.IUserDtoService;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.exception.field.LoginEmptyException;
import ru.t1.sukhorukova.tm.exception.field.PasswordEmptyException;
import ru.t1.sukhorukova.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.sukhorukova.tm.exception.user.LockedException;
import ru.t1.sukhorukova.tm.dto.model.UserDTO;
import ru.t1.sukhorukova.tm.util.CryptUtil;
import ru.t1.sukhorukova.tm.util.HashUtil;

import java.util.Date;

@Service
@NoArgsConstructor
public final class AuthService implements IAuthService {

    @NotNull
    @Autowired
    private IUserDtoService userService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ISessionDtoService sessionService;

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final UserDTO user) {
        final SessionDTO session = createSession(user);
        sessionService.add(session);
        return getToken(session);
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDTO session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private SessionDTO createSession(@NotNull final UserDTO user) {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return session;
    }

    @NotNull
    @Override
    public String login(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.getLocked()) throw new LockedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new IncorrectLoginOrPasswordException();
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        return getToken(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO validateToken(@Nullable final String token) {
        if (token == null) throw new PermissionException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new PermissionException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull SessionDTO session = objectMapper.readValue(json, SessionDTO.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getLastDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        @NotNull final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new PermissionException();
        if (!sessionService.existsById(session.getId())) throw new PermissionException();
        return session;
    }

    @Override
    @SneakyThrows
    public void logout(@Nullable final String token) {
        if (token == null) throw new NotLoggedInException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new PermissionException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull SessionDTO session = objectMapper.readValue(json, SessionDTO.class);
        sessionService.removeOneById(session.getId());
        System.out.println(sessionService.findAll());
    }

}
