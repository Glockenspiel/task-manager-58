package ru.t1.sukhorukova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.model.TaskDTO;
import ru.t1.sukhorukova.tm.enumerated.TaskSort;

import java.util.List;

public interface ITaskDtoRepository extends IUserOwnerDtoRepository<TaskDTO> {

    @Nullable
    List<TaskDTO> findAll(@NotNull String userId, @NotNull TaskSort sort);

    @Nullable
    List<TaskDTO> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    void removeTasksByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

}
