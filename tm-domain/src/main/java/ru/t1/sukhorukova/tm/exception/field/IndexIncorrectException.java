package ru.t1.sukhorukova.tm.exception.field;

public final class IndexIncorrectException extends AbstractFieldExceprion {

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

}
