package ru.t1.sukhorukova.tm.dto.response.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.dto.response.AbstractResultResponse;

public final class ApplicationErrorResponse extends AbstractResultResponse {

    public ApplicationErrorResponse() {
        setSuccess(false);
    }

    public ApplicationErrorResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
